let comeco = ('Hello World!!')

document.write(comeco)

function escreverNoConsole() {
    console.log('Olá!!')
}

escreverNoConsole()

function podeDirigir(idade, cnh) {
    if (idade >= 18 && cnh == true) {
        console.log('Pode dirigir')
    } else {
        console.log('Não pode dirigir!!')
    }
}

podeDirigir(18, true)


const parOuImpar = (valor) => {
    return valor % 2;
}

console.log(parOuImpar(99))

let n = 5

if(parOuImpar(n)==0) {
    console.log(`O número ${n} É par!`)
    } else {
        console.log(`O número ${n} É ímpar!`)
}